# **web_app_logic.py**

## **Logic Class**

Represents the main brain of the Proxy-Web-App for Kimai. Interacts with instance of Kimai API Client class.

### constructor

```python
def __init__(self, kimai_url):
    Constructor for the Logic class.

    Parameters:
        - kimai_url (str): The root URL of the Kimai instance.
          
    Returns:
        - None

    Usage:
        - Initializes the Logic class with the root URL of the Kimai instance.
```

### kimai_is_available
```python
def kimai_is_available(self):
    Test the availability of the Kimai instance.

    Returns:
        - bool: True if Kimai is available, False otherwise.

    Usage:
        - Used to check the availability of the Kimai instance before performing operations.
```
### login 
```python
def login(self, username, api_token, project_id, activity_id):
    Log in a user with user session.

    Parameters:
        - username (str): The username of the user.
        - api_token (str): The API token for authentication.
        - project_id (int): The ID of the project.
        - activity_id (int): The ID of the activity belonging to the project.

    Returns:
        - str or None: JSON representation of the user session on success, None on failure.

    Usage:
        - Logs in a user, starts a timesheet, and returns user session information.
```
### timesheet_is_active
```python
    def timesheet_is_active(user_session_as_json: str):
    Check if a user session contains an active timesheet.

    Parameters:
        - user_session_as_json (str): JSON representation of the user session.

    Returns:
        - bool: True if an active timesheet is present, False otherwise.

    Usage:
        - Used to determine if a user session contains an active timesheet.
```

### start_timesheet
```python
def start_timesheet(self, user_session_as_json: str, project_id: int, activity_id: int):
    Start a new timesheet for the current user.

    Parameters:
        - user_session_as_json (str): JSON representation of the user session.
        - project_id (int): The ID of the project.
        - activity_id (int): The ID of the activity.

    Returns:
        - str or None: JSON representation of the updated user session on success, None on failure.

    Usage:
        - Starts a new timesheet for the user and returns the updated user session information.

```
### stop_timesheet
```python
def stop_timesheet(self, user_session_as_json: str):
    Stop the active timesheet for the current user.

    Parameters:
        - user_session_as_json (str): JSON representation of the user session.

    Returns:
        - str or None: JSON representation of the updated user session on success, None on failure.

    Usage:
        - Stops the active timesheet for the user and returns the updated user session information.
   
```

## **InputVerification Class**

Provides input verification methods. Generally operations and tests on strings.

### field_not_empty
```python
def field_not_empty(field_content: str):
    Check if a field containing a string is not empty.

    Parameters:
        - field_content (str): The content of the field.

    Returns:
        - bool: True if the field is not empty, False otherwise.

    Usage:
        - Used to verify if a text form field is not empty.
```
### pw_field_not_empty
```python
def pw_field_not_empty(field_content: str):
    Check if a password form field is not empty.

    Parameters:
        - field_content (str): The content of the password field.

    Returns:
        - bool: True if the password field is not empty, False otherwise.

    Usage:
        - Used to verify if a password form field is not empty.
```
 

## **MailSystem class**

Implements a simple mail client for sending notifications.
### constructor
```python
def __init__(self, addr, port, pw, username):
    Constructor for the MailSystem class.

    Parameters:
        - addr (str): The address of the mail server.
        - port (int): The port of the mail server.
        - pw (str): The password for mail server authentication.
        - username (str): The username for mail server authentication.
        
    Returns:
        - None

    Usage:
        - Initializes the MailSystem class with mail server connection details.
```
### send_mail
```Python
def send_mail(self, subject, to_addr, from_addr, reply_addr, mail_content):
    Send an email.

    Parameters:
        - subject (str): The subject of the email.
        - to_addr (str): The recipient's email address.
        - from_addr (str): The sender's email address.
        - reply_addr (str): The reply-to email address.
        - mail_content (str): The content of the email.

    Returns:
        - bool: True if the email was sent successfully, False otherwise.

Usage:
        - Sends an email with specified details.
```
### notify_active_timesheets
```python
def notify_active_timesheets(self, to_addr, from_addr, reply_addr, count_active_ts):
    Notify about active timesheets.

    Parameters:
        - to_addr (str): The recipient's email address.
        - from_addr (str): The sender's email address.
        - reply_addr (str): The reply-to email address.
        - count_active_ts (int): The count of active timesheets.

    Returns:
        - bool: True if the notification was sent successfully, False otherwise.
```


## **Config Class**

Handles the configuration file for the application.
### constructor
```python
def __init__(self, configfile):
    Constructor for the Config class.

    Parameters:
        - configfile (str): The path to the configuration file.

    Returns:
        - None

    Usage:
        - Initializes the Config class with the path to the configuration file.
        
```

### load_config
```python
def load_config(self):
    Load the configuration from the file system.

    Returns:
        - bool: True if the configuration is loaded successfully, False otherwise.

    Usage:
        - Loads the configuration from the specified file path.
           
```

### verify_config
```python
def verify_config(self):
    Verify the loaded configuration. Check whether every option has a value.

    Returns:
        - bool: True if the configuration is valid, False otherwise.

    Usage:
        - Checks if the loaded configuration is valid.
         
```

### verify_options
```python
def verify_options(self):
    Verify the configuration options. Check whether every option has a non-None value.

    Returns:
        - bool: True if all options have values, False otherwise.

    Usage:
        - Checks if all configuration options have valid values.
        
```

### load_options
```python
def load_options(self):
    Load final option values from the configuration file to the variables of the instance.

    Returns:
        - bool: True if options are loaded successfully, False otherwise.

    Usage:
        - Loads configuration options from the file system.
          
```

### options
```python
def options(self):
    Get the dictionary containing all configuration options.

    Returns:
        - dict: Dictionary containing configuration options.

    Usage:
        - Retrieves the dictionary of configuration options.
```

## **RateLimit Class**

Represents rate limiting functions to mitigate extensive or abusive use of web app endpoints.

### constructor
```python
def __init__(self, flask_app, memory_backend):
    Constructor for the RateLimit class.

    Parameters:
        - flask_app: The Flask application.
        - memory_backend (str): String referencing the memory backend.

    Returns:
        - None

    Usage:
        - Initializes the RateLimit class with Flask application and memory backend details.
         
```

### get_limiter
```python
def get_limiter(self):
    Get the Limiter object.

    Returns:
        - Limiter: The Limiter object.

    Usage:
        - Retrieves the Limiter object.
        
```

## **UserSession Class**
Represents the custom session object stored in the user's cookie.

### constructor
```python
def __init__(self, username=None, userid=None, api_token=None):
    Constructor for the UserSession class.

    Parameters:
        - username (str): The username of the user.
        - userid (str): The user ID.
        - api_token (str): The API token for authentication.

    Returns:
        - None

    Usage:
        - Initializes the UserSession class with user session details.
           
```

### set_active_timesheet_id
```python
def set_active_timesheet_id(self, tsid):
    Set the ID for the currently active timesheet.

    Parameters:
        - tsid: The ID for the active timesheet.

    Returns:
        - None

    Usage:
        - Sets the ID for the currently active timesheet.
        
```

### get_active_timesheet_id
```python
def get_active_timesheet_id(self):
    Get the ID for the currently active timesheet.

    Returns:
        - int: The ID for the active timesheet.

    Usage:
        - Retrieves the ID for the currently active timesheet.
        
```

### get_username
```python
def get_username(self):
    Get the username.

    Returns:
        - str: The username.

    Usage:
        - Retrieves the username.
        

```

### get_api_token
```python
def get_api_token(self):
    Get the API token.

    Returns:
        - str: The API token.

    Usage:
        - Retrieves the API token.
        
```

### to_json
```python
    Dumps the object to JSON.

    Returns:
        - str: JSON representation of the UserSession object (current instance).

    Usage:
        - Converts the object to a JSON string for storage.
        
```

### from_json
```python
def from_json(self, json_string):
    Loads object data from a JSON string into current class instance.

    Parameters:
        - json_string (str): JSON representation of the object.

    Returns:
        - None: Modifies the object's internal state.

    Usage:
        - Loads object data from a JSON string. 
```
**Author**: Niklas Weber


