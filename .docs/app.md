# **app.py**

### rate_limited
```python
def rate_limited(e):
    Render 429 Error page for rate-limited requests.
        
    Parameters:
        - e (Exception): The exception.

    Returns:
        - tuple: 429 Error page template and status code 429.

    Usage:
        - Renders the 429 Error page for rate-limited requests.
```

### page_not_found
```python
def page_not_found(e):
    Render custom 404 Error page.
    
    Parameters:
        - e (Exception): The exception.

    Returns:
        - tuple: 404 Error page template and status code 404.

    Usage:
        - Renders the custom 404 Error page.   
```

### method_not_found
```python
def method_not_found(e):
    Render custom 405 Error page for Method Not Found.
        
    Parameters:
        - e (Exception): The exception.

    Returns:
        - tuple: 405 Error page template and status code 405.

    Usage:
        - Renders the custom 405 Error page for Method Not Found.
```

### server_error(500)
```python
def server_error(e):
    Render custom 500 Error page for Internal Server Error.

    Parameters:
        - e (Exception): The exception.

    Returns:
        - tuple: 500 Error page template and status code 500.

    Usage:
        - Renders the custom 500 Error page for Internal Server Error. 
```

### server_error(502)
```python
def server_error(e):
    Render custom 502 Error page for Gateway Timeout (Kimai Instance Unavailable).
        
    Parameters:
        - e (Exception): The exception.
    
    Returns:
        - tuple: 502 Error page template and status code 502.

    Usage:
        - Renders the custom 502 Error page for Gateway Timeout (Kimai Instance Unavailable).
    
```

### handle_csrf_error
```python
def handle_csrf_error(e):
    Render custom CSRF Token Error page.

    Parameters:
        - e: CSRFError object.

    Returns:
        - tuple: CSRF Error page template and status code 400.

    Usage:
        - Renders the custom CSRF Token Error page.
    

```

### user_session_required
```python
def user_session_required(f):
    Wrapper function to check whether the user has a valid session.

    Returns:
        - function: Wrapped function.

    Usage:
        - Checks if the user has a valid session before executing the wrapped function.
 
```

### no_session required
```python
def no_session_required(f):
    Wrapper function to check whether the user has no valid session yet.

    Parameters:
        - f (function): The function to be wrapped.
    
    Returns:
        - function: Wrapped function.

    Usage:
        - Checks if the user has no valid session yet before executing the wrapped function.
```

### check_kimai_availability
```python
def check_kimai_availability():
    Test whether the corresponding Kimai instance is online.
    
    Returns:
        - None: If Kimai is available.
        - abort(502): If Kimai is not available, abort with a 502 error.
    
    Usage:
        - Checks whether the Kimai instance is online before serving pages.

```

### home
```python
def home():  
    Default route (root) that contains the login form. Only accepts GET.

    Returns:
        - index.html: The rendered html template for the home page.

    Usage:
        - Renders the default route with the login form.
    
```

### login
```python
def login():
    Route to log in the user, validate credentials, and create a session. Only accepts POST.

    Returns:
        - redirect: Redirects to the start_timesheet() method on success or default route on failure.

    Usage:
        - Handles user login and session creation.
```

### logout
```python
def logout():
    Route to log out the user. Only accepts GET.

    Returns:
        - redirect: Redirects to default route.

    Usage:
        - Handles user logout and session removal.
```

### start_timesheet
```python
def start_timesheet():
    Route to start the timesheet. Only accepts GET.

    Returns:
        - start_time.html or redirect: Renders html template or redirects to stop_timesheet() method when already active.

    Usage:
        - Handles starting the timesheet.
    
```

### stop_timesheet
```python
def stop_timesheet():
    Route to stop the timesheet. Only accepts GET.

    Returns:
        - stop_time.html or redirect: Renders html template or redirects to start_timesheet() method.

    Usage:
        - Handles stopping the timesheet.
```
**Author**: Niklas Weber