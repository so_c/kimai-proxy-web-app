
# **Welcome to the Proxy-Web-App for Kimai (PWAK)  Code Documentation!**

## **Purpose**

This piece of Software was created during a student project at the University of Applied Science Mittweida.

It is a small web application written in Python with Flask which serves as a proxy between the User (via Web UI) and the API of an Kimai instance.
The program is meant to only start and stop the working time for a given user. For that it comes with a minimal web UI to focus on what really matters.

One specific use case is giving access to time-tracking to employees working outside the companies internal network. No special end device configuration needed. This application needs to be deployed between intranet and internet and forwards the requests to Kimai.

## **Prerequisites**

Before using the Proxy-Web-App for Kimai, ensure the following prerequisites are met:

- Working Kimai instance with available API endpoints
- Docker & Docker Compose installed on the host, user with sudo privileges
- SSL: DNS A/AAA-Record set to the IP of the host machine
- When installed manually: Webserver WSGI-capable, Python & pip, uWSGI, or similar

## **Installation**

Follow these steps for a fast and easy setup of PWAK (Proxy-Web-App for Kimai) with minimal effort and automatic SSL certificate installation and renewal using Docker and Docker Compose:

1. Go into your desired working directory.
2. Copy over `compose.yaml` and `config_compose.ini`.
3. Adjust `config_compose.ini` to your needs:
    - Set the right (cookie) domain under which PWAK will be available.
    - By default, memcached is used as memory backend for rate limiting, ensure its URI matches the docker container name.
    - Configure the correct Kimai instance URL with no trailing slash / at the end.

4. In the `compose.yaml`: Replace all occurrences of `pwak.foo.tld` with your domain (must match the DNS-Record).
5. Make sure port 80 + 443 is open to have automatic SSL going to work.
6. Start the application: `docker compose up -d`.

## **Documentation**

The complete code documentation for the Proxy-Web-App for Kimai is available in several sections:

- [App](app.md): Description of the main application and its features.
- [Web App Logic](web_app_logic.md): Details about the business logic of the Proxy-Web-App for Kimai.
- [Kimai API Client](kimai_api_client.md): Information about the API client for Kimai integration.

## **Support**

If you encounter issues or have questions regarding the application's usage, do not hesitate to contact [Jakob Henze](mailto:jhenze2@hsmw-de) or [Niklas Weber](mailto:nweber5@hsmw.de).

Thank you for using the PWAK!!!

**Author**: Niklas Weber