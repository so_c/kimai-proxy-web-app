# **kimai_api_client.py**

## **KimaiAPIClient Class**

Provides functionality to interact with the Kimai REST-API.

### constructor 
```python
def __init__(self, username, api_token, kimai_url: str):
    Constructor of the KimaiAPIClient class.

    Parameters:
        - username (str): The username for Kimai authentication.
        - api_token (str): The API token for Kimai authentication.   
        - kimai_url (str): The URL of the Kimai instance.   

    Returns:
        - None 

    Usage: 
        - Initializes the KimaiAPIClient with specified credentials and Kimai instance URL.
```

### check_credentials
```python
def check_credentials(self):
    Test for correct user credentials.

    Returns:
        - int: Unique user ID on success, 0 on failure.

    Usage:
        - Checks if the provided credentials are correct.
```

### start_timesheet
```python
def start_timesheet(self, project_id, activity_id):
    Create and start a timesheet.

    Parameters:
        - project_id (int): The ID of the project.
        - activity_id (int): The ID of the activity.

    Returns:
        - int: Timesheet ID on success, 0 on failure.

    Usage:
        - Starts a timesheet for the specified project and activity. 
```

### stop_timesheet
```python
def stop_timesheet(self, timesheet_id):
    Stop an active timesheet.

    Parameters:
        - timesheet_id (int): The ID of the timesheet to stop.

    Returns:
        - bool: True on success, False on failure.

    Usage:
        - Stops an active timesheet with the specified ID.
```

### test_connection
```python
def test_connection(self):
    Test the general connection to the Kimai instance without any credentials.

    Returns:
        - bool: True on success, False on failure.

    Usage:
        - Tests the general connection to Kimai login.
        
```
### user_timesheet_active
```python
def user_timesheet_active(self) -> tuple[int, int, int]:
    Check for an active timesheet.

    Returns:
        - tuple[int, int, int]: Tuple of timesheet_id, activity_id, and project_id if active, else 0.

    Usage:
        - Checks for an active timesheet and returns relevant information.
        
```

### get_username
```python
def get_username(self):
    Getter for the username of current instance of class.

    Returns:
        - str: The username.

    Usage:
        - Retrieves the username associated with the KimaiAPIClient instance.
        
```

### get_token
```python
def get_token(self):
    Getter for the API token of current instance of class.

    Returns:
        - str: The API token.

    Usage:
        - Retrieves the API token associated with the KimaiAPIClient instance.
        
```
**Author**: Niklas Weber
