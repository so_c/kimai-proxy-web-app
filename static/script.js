//called page specific
function onClickStart(id, formID){
        document.body.style.transition = 'background-color 0.5s ease';
        document.body.style.backgroundColor = '#19B173';
        document.getElementById(id).style.transition = 'background-color 1.5s ease';
        document.getElementById(id).style.backgroundColor = 'grey';
        document.getElementById(id).style.fontWeight = 'bold';
        document.getElementById(id).style.cursor = 'default';
        document.getElementById(id).textContent = "STARTE...";
        document.getElementById(formID).submit();
}

function onClickStop(id, formID){
        document.body.style.transition = 'background-color 0.5s ease';
        document.body.style.backgroundColor = 'white';
        document.getElementById(id).style.transition = 'background-color 1.5s ease';
        document.getElementById(id).style.backgroundColor = 'grey';
        document.getElementById(id).style.fontWeight = 'bold';
        document.getElementById(id).style.cursor = 'default';
        document.getElementById(id).textContent = "STOPPE...";
        document.getElementById(formID).submit();
}

// always executed at page load
function fadeOutFlash(){
        const fadeTarget = document.getElementById("flash_m");
        let fadeEffect = setInterval(function () {
                if (fadeTarget != null){
                        if (!fadeTarget.style.opacity) {
                                fadeTarget.style.opacity = "1";
                        }
                        if (fadeTarget.style.opacity > 0) {
                                fadeTarget.style.opacity -= "0.1";
                        } else {
                                clearInterval(fadeEffect);
                        }
                }
                }, 50);
}
setTimeout(function(){ fadeOutFlash() }, 4000);