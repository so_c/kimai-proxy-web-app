# Proxy-Web-App for Kimai (PWAK)

## Purpose
This piece of Software was created during a student project at the University of Applied Science Mittweida.

It is a small web application written in Python with [Flask](https://flask.palletsprojects.com) which serves as a proxy between the User (via Web UI) and the API of an [Kimai](https://kimai.org) instance. 
The program is meant to only start and stop the working time for a given user. For that it comes with a minimal web UI to focus on what really matters.

One specific use case is giving access to time-tracking to employees working outside the companies internal network. No special end device configuration needed. This application needs to be deployed between intranet and internet and forwards the requests to Kimai.

## Prerequisites
- Working Kimai instance with available [API](https://www.kimai.org/documentation/rest-api.html) endpoints
- Docker & Docker Compose installed on the host, user with sudo privileges
- SSL: DNS A/AAA-Record set to the IP of the host machine
- When installed manually: Webserver WSGI-capable, Python & pip, uWSGI or similar

## Installation
The following steps describe a fast and easy way to set up PWAK with minimal effort and automatic SSL certificate installation and renewal using Docker and Docker Compose.

If you have specific requirements please stick to a manual installation. These [Docs](https://flask.palletsprojects.com/en/3.0.x/deploying/) might be a good starting point.

1. Go into your desired working directory.
2. Copy over `compose.yaml` and `config_compose.ini`.
3. Adjust `config_compose.ini` to your needs.
    1. Set the right (cookie) domain under which PWAK will be available.
    2. By default, memcached is used as memory backend for rate limiting, ensure its URI matches the docker container name.
    3. Configure the correct Kimai instance URL with no trailing slash `/` at the end.
4. In the `compose.yaml`: Replace all occurrences of `pwak.foo.tld` with your domain (must match the DNS-Record).
5. Make sure port 80 + 443 is open to have automatic SSL going to work.
6. Start the application: `docker compose up -d`

## Troubleshooting
`docker ps` shows all running containers with some information such as the container name.

To inspect possible errors or the logs in general use `docker logs <container name>` to have a look at the inside.
For further Debugging: Add option `-f` to the command to get a live view of all new logs.
