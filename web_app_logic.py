import base64
import configparser
import datetime
import json
import os
import smtplib
import ssl
import textwrap
from configparser import ConfigParser
import cryptography
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from kimai_api_client import KimaiAPIClient
from flask import current_app
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import pymemcache


# Class represents main brain of application: contains the logic to handle user input and pass it to kimai-api-client
#   test kimai availability
#   login and verify given credentials at kimai instance
#   start and stop the timesheet
#   test for active timesheet
class Logic:
    """
    Represents the main brain of the Proxy Web App for Kimai. Interacts with instance of KimaiAPIClient class.
    """

    # constructor
    def __init__(self, kimai_url):
        """
        Constructor for the Logic class.

        Parameters:
          kimai_url (str): The root URL of the Kimai instance
        """
        self._kimai_root_url = kimai_url

    # test kimai instance availability against its root url
    def kimai_is_available(self):
        """
        Test the availability of the Kimai instance.

        Returns:
        - bool: True if Kimai is available, False otherwise.
        """
        api = KimaiAPIClient("", self._kimai_root_url)
        is_available = api.test_connection()
        if is_available is True:
            return True
        else:
            return False

    # method to check credentials of user trying to log in
    # on success return user session instance as json, on failure: return None
    def login(self, api_token, project_id, activity_id, username=None):
        """
        Log in a user with user session.

        Parameters:
        - api_token (str): The API token for authentication.
        - project_id (int): The ID of the project.
        - activity_id (int): The ID of the activity belonging to the project.
        - optional: username (str, default: None): The username for old Kimai api authentication method.

        Returns:
        - str or None: JSON representation of the user session on success, None on failure.
        """
        # create api client instance
        api = KimaiAPIClient(api_token, self._kimai_root_url, username=username)
        # call api clients login method and check return value (user_id)
        user_id = api.check_credentials()
        if user_id != 0:
            # retrieve username
            username = api.get_username()
            # create user session instance
            us = UserSession(username, user_id, api_token)
            # check for active ts started via application and write it into user session as active ts_id
            # on error: fails silently and doesn't write active ts into user session - this is expected behaviour
            # as it is not mission-critical
            active_ts_id = api.user_timesheet_active()
            if active_ts_id[0] != 0 and active_ts_id[1] == activity_id and active_ts_id[2] == project_id:
                us.set_active_timesheet_id(active_ts_id[0])
                return us.to_json()
            else:
                return us.to_json()
        else:
            return None

    # test whether user session contains active timesheet, used for start_ stop_ timesheet funcs
    @staticmethod
    def timesheet_is_active(user_session_as_json: str):
        """
         Check if a user session contains an active timesheet.

        Parameters:
        - user_session_as_json (str): JSON representation of the user session.

        Returns:
        - bool: True if an active timesheet is present, False otherwise.
        """
        # create user session instance
        us = UserSession(us_json_data=user_session_as_json)
        # retrieve value for active_timesheet_id
        # if 0 return false, else: return true (active timesheet present)
        if us.get_active_timesheet_id() == 0:
            return False
        else:
            return True

    def start_timesheet(self, user_session_as_json: str, project_id: int, activity_id: int, old_api=0):
        """
         Start a new timesheet for the current user.

        Parameters:
        - user_session_as_json (str): JSON representation of the user session.
        - project_id (int): The ID of the project.
        - activity_id (int): The ID of the activity.
        - optional: old_api (int): defaults to 0, called with 1 it will use the old Kimai API

        Returns:
        - str or None: JSON representation of the updated user session on success, None on failure.
        """
        # pass user session json into timesheet_is_active() and test whether there is an active timesheet
        # if false: continue & load user session json into instance // if true: return None
        if self.timesheet_is_active(user_session_as_json):
            return None
        else:
            # create user session instance
            us0 = UserSession(us_json_data=user_session_as_json)
            # create api client instance and retrieve username and api token
            if old_api == 1:
                api = KimaiAPIClient(us0.get_api_token(), self._kimai_root_url, us0.get_username())
            else:
                api = KimaiAPIClient(us0.get_api_token(), self._kimai_root_url)
            # call api method to start timesheet, if success: write returned timesheet_id into user session instance
            # if no success: return None
            # return updated user session as json on full success
            ts_id = api.start_timesheet(project_id, activity_id)
            if ts_id != 0:
                us0.set_active_timesheet_id(ts_id)
                return us0.to_json()
            else:
                return None

    def stop_timesheet(self, user_session_as_json: str, old_api=0):
        """
        Stop the active timesheet for the current user.

        Parameters:
        - user_session_as_json (str): JSON representation of the user session.
        - optional: old_api (int): defaults to 0, called with 1 it will use the old Kimai API

        Returns:
        - str or None: JSON representation of the updated user session on success, None on failure.
        """
        # pass user session json into timesheet_is_active() and test whether there is an active timesheet
        # if true: continue & load user session json into instance // if false: return None
        if not self.timesheet_is_active(user_session_as_json):
            return None
        else:
            # create user session instance
            us0 = UserSession(us_json_data=user_session_as_json)
            # create api client instance and retrieve username and api token
            if old_api == 1:
                api = KimaiAPIClient(us0.get_api_token(), self._kimai_root_url, us0.get_username())
            else:
                api = KimaiAPIClient(us0.get_api_token(), self._kimai_root_url)
            # call api method to stop active timesheet, if success: write 0 as timesheet_id into user session instance
            # if no success: abort, return None
            # return updated user session as json on full success
            if api.stop_timesheet(us0.get_active_timesheet_id()):
                us0.set_active_timesheet_id(0)
                return us0.to_json() 
            else:
                return None


# class used to namespace helper functions, most of them might be static methods
class InputVerification:
    """
    Provides input verification methods. Generally operations and tests on strings.
    """

    # checks whether there is some input in a text form field
    @staticmethod
    def field_not_empty(field_content: str):
        """
         Check if a field containing a string is not empty.

        Parameters:
        - field_content (str): The content of the field.

        Returns:
        - bool: True if the field is not empty, False otherwise.
        """
        if field_content is None:
            return False
        s = field_content.strip()
        if len(s) == 0:
            return False
        else:
            return True

    # checks whether there is some input in a password form field.
    # function doesnt use str.strip() as whitespace could also be part of the password
    @staticmethod
    def pw_field_not_empty(field_content: str):
        """
        Check if a password form field is not empty.

        Parameters:
        - field_content (str): The content of the password field.

        Returns:
        - bool: True if the password field is not empty, False otherwise.
        """
        if field_content is None:
            return False
        if len(field_content) == 0:
            return False
        else:
            return True


# implements simple mail client to send notifications
# intended to be used at server exit when there are still active timesheets
# IMPORTANT: this class is not in use at the moment, needs further implementation into the logic
class MailSystem:
    """
    Implements a simple mail client for sending notifications.
    This class is not in use at the current stage of the application.
    """

    # constructor with values for mail server connection
    # mail server has to speak the starttls protocol
    def __init__(self, addr, port, pw, username):
        """
        Constructor for the MailSystem class.

        Parameters:
        - addr (str): The address of the mail server.
        - port (int): The port of the mail server.
        - pw (str): The password for mail server authentication.
        - username (str): The username for mail server authentication.
        """
        self._server_port = port
        self._server_addr = addr
        self._server_pw = pw
        self._server_username = username
        try:
            self._ssl_context = ssl.create_default_context()
        except ssl.SSLError as e:
            print("Error in SSL Module: " + e.library + " /// Grund:" + e.reason)

    # base method for sending mails
    def send_mail(self, subject, to_addr, from_addr, reply_addr, mail_content):
        """
        Send an email.

        Parameters:
        - subject (str): The subject of the email.
        - to_addr (str): The recipient's email address.
        - from_addr (str): The sender's email address.
        - reply_addr (str): The reply-to email address.
        - mail_content (str): The content of the email.

        Returns:
        - bool: True if the email was sent successfully, False otherwise.
        """
        try:
            with smtplib.SMTP(self._server_addr, self._server_port) as mailserver:
                mailserver.starttls(context=self._ssl_context)
                mailserver.login(self._server_username, self._server_pw)
                full_mail = "From: {}\nTo: {}\nSubject: {}\nReply-To: {}\n\n{}".format(from_addr, to_addr, subject,
                                                                                       reply_addr, mail_content)
                mailserver.sendmail(from_addr, to_addr, full_mail.encode("utf-8"))
                return True
        except smtplib.SMTPException as e:
            print("Error in SMTP Module: " + e.strerror)
            return False

    def notify_active_timesheets(self, to_addr, from_addr, reply_addr, count_active_ts):
        """
         Notify about active timesheets.

        Parameters:
        - to_addr (str): The recipient's email address.
        - from_addr (str): The sender's email address.
        - reply_addr (str): The reply-to email address.
        - count_active_ts (int): The count of active timesheets.

        Returns:
        - bool: True if the notification was sent successfully, False otherwise.
        """
        this_year = datetime.date.today().year
        subject = f"Kimai Proxy-Web-App gestoppt! // Es gibt {count_active_ts} aktive Zeiterfassungen."
        content = f"""
        Der Proxy-Web-App-Server wurde gestoppt. Es gibt noch aktive Zeiterfassungen (Timesheets).
        Bitte stoppen sie diese manuell oder starten sie die Web-App wieder.
        
        Anzahl aktiver Timesheets: {count_active_ts}
        
        
        
        *** Diese Nachricht wurde automatisch generiert. ***
        
        Proxy-Web-App fuer https://kimai.org
        {this_year}. All Rights Reserved."""

        if self.send_mail(subject, to_addr, from_addr, reply_addr, textwrap.dedent(content)):
            return True
        else:
            return False


# Config Class to handle the Configuration file
#   read config from file system
#   load and verify config parameters
#   set class variables and create getters for it
class Config:
    """
    Handles the configuration file for the application.
    """

    # constructor of class
    def __init__(self, configfile):
        """
         Constructor for the Config class.

        Parameters:
        - configfile (str): The path to the configuration file.
        """
        self._config_path = os.path.join("instance", configfile)
        self._cp = ConfigParser()

        # config variables for all config options (need to match with config.ini)
        self._config_options = {
            "secretkey": None,
            "cookiesecure": None,
            "jshttponly": None,
            "cookiedomain": None,
            "sessionlifetime": None,
            "samesite": None,
            "behindproxy": None,
            "memcached": None,
            "memcachedserver": None,
            "memcachedport": None,
            "limitermemory": None,
            "apptitle": None,
            "imprinturl": None,
            "admincontact": None,
            "privacyname": None,
            "privacystreet": None,
            "privacyziploc": None,
            "privacyceo": None,
            "privacycontact": None,
            "kimaihttps": None,
            "kimaiurl": None,
            "kimaifullurl": None,
            "kimaiprojectid": None,
            "kimaiactivityid": None,
            "mail_server_addr": None,
            "mail_server_port": None,
            "mail_server_username": None,
            "mail_server_password": None,
            "mail_server_to_addr": None,
            "mail_server_from_addr": None,
            "mail_server_reply_addr": None
        }

    # load the config from file system (instance/config.ini) into the object (_cp)
    def load_config(self):
        """
        Load the configuration from the file system into the class instance.

        Returns:
        - bool: True if the configuration is loaded successfully, False otherwise.
        """
        try:
            retval = self._cp.read(self._config_path, "UTF-8")
        except configparser.Error as e:
            print("Error in ConfigParser Module [load_config() from Config Class]: " + e.message)
            return False
        if len(retval) == 0:
            return False
        return True

    # briefly read config to check whether every option has a value
    def verify_config(self):
        """
        Verify the loaded configuration. Check whether every option has a value.

        Returns:
        - bool: True if the configuration is valid, False otherwise.
        """
        try:
            for section in self._cp.sections():
                for key, value in self._cp.items(section):
                    if value == "":
                        return False
            return True
        except configparser.Error as e:
            print("Error in ConfigParser Module [verify_config() from Config Class]: " + e.message)
            return False

    # check over option if none of them is 'None' -> None is default and should not exist as value after load_options()
    def verify_options(self):
        """
        Verify the configuration options. Check whether every option has a non-None value

        Returns:
        - bool: True if all options have values, False otherwise.
        """
        try:
            for key, value in self._config_options.items():
                if value is None:
                    return False
            return True
        except configparser.Error as e:
            print("Error in ConfigParser Module [verify_options() from Config Class]: " + e.message)
            return False
        except ValueError as e:
            print("Error in ConfigParser Module [verify_options() from Config Class]: ", e)
            return False

    # load options from config to class variables
    def load_options(self):
        """
        Load final option values from the configuration file to the variables of the instance.

        Returns:
        - bool: True if options are loaded successfully, False otherwise.
        """
        try:
            self._config_options["secretkey"] = self._cp.get("FLASK", "secret_key", fallback=None)
            self._config_options["cookiesecure"] = self._cp.getboolean("FLASK", "cookie_secure", fallback=None)
            self._config_options["jshttponly"] = self._cp.getboolean("FLASK", "js_http_only", fallback=None)
            self._config_options["cookiedomain"] = self._cp.get("FLASK", "cookie_domain", fallback=None)
            self._config_options["sessionlifetime"] = self._cp.getint("FLASK", "session_lifetime", fallback=None)
            self._config_options["samesite"] = self._cp.get("FLASK", "same_site", fallback=None)
            self._config_options["behindproxy"] = self._cp.getboolean("FLASK", "behind_proxy", fallback=None)
            self._config_options["memcached"] = self._cp.getboolean("FLASK", "memcached", fallback=None)
            self._config_options["memcachedserver"] = self._cp.get("FLASK", "memcached_server", fallback=None)
            self._config_options["memcachedport"] = self._cp.getint("FLASK", "memcached_port", fallback=None)
            if (not any(x is None for x in (self._config_options["memcached"], self._config_options["memcachedserver"],
                                            self._config_options["memcachedport"]))
                    and self._config_options["memcached"] is True):
                self._config_options["limitermemory"] = ("memcached://" + self._config_options["memcachedserver"] + ":"
                                                         + str(self._config_options["memcachedport"]))
            else:
                self._config_options["limitermemory"] = "memory://"

            self._config_options["apptitle"] = self._cp.get("APPLICATION", "title", fallback=None)
            self._config_options["imprinturl"] = self._cp.get("APPLICATION", "imprint_url", fallback=None)
            self._config_options["admincontact"] = self._cp.get("APPLICATION", "admin_contact", fallback=None)
            self._config_options["privacyname"] = self._cp.get("APPLICATION", "privacy_name", fallback=None)
            self._config_options["privacystreet"] = self._cp.get("APPLICATION", "privacy_street_no", fallback=None)
            self._config_options["privacyziploc"] = self._cp.get("APPLICATION", "privacy_zip_location", fallback=None)
            self._config_options["privacyceo"] = self._cp.get("APPLICATION", "privacy_ceo", fallback=None)
            self._config_options["privacycontact"] = self._cp.get("APPLICATION", "privacy_contact", fallback=None)

            self._config_options["kimaihttps"] = self._cp.getboolean("KIMAI", "use_https", fallback=None)
            self._config_options["kimaiurl"] = self._cp.get("KIMAI", "kimai_url", fallback=None)
            if self._config_options["kimaihttps"] is True and self._config_options["kimaiurl"] is not None:
                self._config_options["kimaifullurl"] = "https://" + self._config_options["kimaiurl"]
            if self._config_options["kimaihttps"] is False and self._config_options["kimaiurl"] is not None:
                # noinspection HttpUrlsUsage
                self._config_options["kimaifullurl"] = "http://" + self._config_options["kimaiurl"]
            self._config_options["kimaiprojectid"] = self._cp.getint("KIMAI", "kimai_project_id", fallback=None)
            self._config_options["kimaiactivityid"] = self._cp.getint("KIMAI", "kimai_activity_id", fallback=None)

            self._config_options["mail_server_addr"] = self._cp.get("EMAIL", "smtp_server", fallback=None)
            self._config_options["mail_server_port"] = self._cp.getint("EMAIL", "smtp_port", fallback=None)
            self._config_options["mail_server_username"] = self._cp.get("EMAIL", "smtp_username", fallback=None)
            self._config_options["mail_server_password"] = self._cp.get("EMAIL", "smtp_password", fallback=None)
            self._config_options["mail_server_to_addr"] = self._cp.get("EMAIL", "to_address", fallback=None)
            self._config_options["mail_server_from_addr"] = (self._cp.get("EMAIL", "from_name", fallback=None) + " <" +
                                                             self._cp.get("EMAIL", "from_address", fallback=None) + ">")
            self._config_options["mail_server_reply_addr"] = self._cp.get("EMAIL", "reply_address", fallback=None)
        except configparser.Error as e:
            print("Error in ConfigParser Module [load_options() from Config Class]: " + e.message)
            return False
        return True

    # getter for dict with config options
    def options(self):
        """
        Get the dictionary containing all configuration options.

        Returns:
        - dict: Dictionary containing configuration options.
        """
        return self._config_options


# represents the rate limiting functions to mitigate extensive or abusive use of the web app endpoints
class RateLimit:
    """
    Represents rate limiting functions to mitigate extensive or abusive use of web app endpoints.
    """

    # constructor which needs the flask app (usually: app) and the string referencing the memory backend
    # rate limiter is constructed with default limits:
    #   max. 10 000 requests per day
    #   max. 900 per hour
    #   max. 5 per second
    def __init__(self, flask_app, memory_backend):
        """
        Constructor for the RateLimit class.

        Parameters:
        - flask_app: The Flask application.
        - memory_backend (str): String referencing the memory backend.
        """
        self._limiter = Limiter(
            get_remote_address,
            app=flask_app,
            default_limits=["10000 per day", "900 per hour", "5 per second"],
            storage_uri=memory_backend,
            storage_options={}
        )

    # returns the Limiter object
    def get_limiter(self):
        """
         Get the Limiter object.

        Returns:
        - Limiter: The Limiter object.
        """
        return self._limiter


# this class represents the custom session object which is stored in the cookie of the users browser
class UserSession:
    """
    Represents the custom session object stored in the user's cookie.
    """
    # constructor, called to create the object (usually after login was successful and credentials get saved in it)
    def __init__(self, username=None, userid=None, api_token=None, us_json_data=None):
        """
        Constructor for the UserSession class.

        Parameters:
        - username (str): The username of the user.
        - userid (str): The user ID.
        - api_token (str): The API token for authentication.
        - us_json_data (str): The encrypted JSON representation of the UserSession object.

        Exceptions:
        - ValueError: If provided JSON data is invalid or could not be decrypted.
        """
        self._username = username
        self._userid = userid
        self._api_token = api_token
        self._active_timesheet_id = 0

        if us_json_data is not None:
            if self._from_json(us_json_data) is None:
                raise ValueError("Invalid JSON data.")

    # sets the id for the currently active timesheet
    def set_active_timesheet_id(self, tsid):
        """
        Set the ID for the currently active timesheet.

        Parameters:
        - tsid: The ID for the active timesheet.
        """
        self._active_timesheet_id = tsid

    # gets the id for the currently active timesheet
    def get_active_timesheet_id(self):
        """
        Get the ID for the currently active timesheet.

        Returns:
        - int: The ID for the active timesheet.
        """
        return self._active_timesheet_id

    # gets the username
    def get_username(self):
        """
        Get the username.

        Returns:
        - str: The username.
        """
        return self._username

    # gets the api token
    def get_api_token(self):
        """
        Get the API token.

        Returns:
        - str: The API token.
        """
        return self._api_token

    # dumps the object to json to be saved (in the session cookie)
    def to_json(self):
        """
        Dump the object to JSON and encrypt it with Fernet algorithm.

        Returns:
        - str: JSON representation of the UserSession object (current instance).
        """
        try:
            kdf = PBKDF2HMAC(
                algorithm=hashes.SHA256(),
                length=32,
                salt=current_app.secret_key.encode(),
                iterations=480000,
            )
            fernet_key = base64.urlsafe_b64encode(kdf.derive(current_app.secret_key.encode()))
            fernet = Fernet(fernet_key)
            return fernet.encrypt(json.dumps(self, default=lambda o: o.__dict__).encode())
        except TypeError as e:
            print("Error with Encryption in UserSession Module [to_json() from UserSession Class]: ", e)
            return None
        except json.JSONDecodeError as e:
            print("Error in UserSession Module [to_json() from UserSession Class]: ", e.msg)
            return None

    # loads the object from json (out of the session cookie)
    def _from_json(self, json_string):
        """
         Load object data from a Fernet encrypted JSON string into current class instance.

    Parameters:
        json_string (str): JSON representation of the object.

    Returns:
        Int: On Success. Modified the object's internal state successfully.
        None: Only on error, when an exception is raised.
        """
        try:
            kdf = PBKDF2HMAC(
                algorithm=hashes.SHA256(),
                length=32,
                salt=current_app.secret_key.encode(),
                iterations=480000,
            )
            fernet_key = base64.urlsafe_b64encode(kdf.derive(current_app.secret_key.encode()))
            fernet = Fernet(fernet_key)
            self.__dict__ = json.loads(fernet.decrypt(json_string).decode())
            return 1
        except cryptography.fernet.InvalidToken as e:
            print("Error with Decryption in UserSession Module [_from_json() from UserSession Class]: ", e)
            return None
        except json.JSONDecodeError as e:
            print("Error in UserSession Module [_from_json() from UserSession Class]: ", e.msg)
            return None
