from functools import wraps
from flask import Flask, render_template, session, flash, redirect, url_for, request, abort, jsonify, g
from flask_wtf.csrf import CSRFProtect, CSRFError
from werkzeug.middleware.proxy_fix import ProxyFix
from web_app_logic import Config, RateLimit, InputVerification, Logic, UserSession
from flask_talisman import Talisman

# INIT I
# create the flask application
app = Flask(__name__)

# INIT II
# load app settings
app_config = Config("config.ini")
# load config and abort if error
if not app_config.load_config():
    raise RuntimeError("Error in loading config.ini. Abort.")
# check config briefly (all settings must have a value) and abort if error
if not app_config.verify_config():
    raise RuntimeError("Error in config.ini. Abort.")
# load options from config.ini into class instance
if app_config.load_options():
    # check if none of the options is 'None' (means that there went something wrong when loading options)
    if not app_config.verify_options():
        raise RuntimeError("Error in config.ini. Abort.")
else:
    raise RuntimeError("Error while reading options from config.ini. Abort.")

# init flask-talisman, handling security response headers and cookie policy for the application
# see: https://github.com/GoogleCloudPlatform/flask-talisman
# some options may be overwritten when defining flask options, this is intended to be in control over some parameters
# during tests, etc.
Talisman(app, force_https=False)

# define flask options
app.config["PERMANENT_SESSION_LIFETIME"] = app_config.options()["sessionlifetime"]
app.config["SESSION_COOKIE_SECURE"] = app_config.options()["cookiesecure"]
app.config["SESSION_COOKIE_HTTPONLY"] = app_config.options()["jshttponly"]
app.config["SESSION_COOKIE_SAMESITE"] = app_config.options()["samesite"]
app.config["SESSION_COOKIE_DOMAIN"] = app_config.options()["cookiedomain"]
app.secret_key = app_config.options()["secretkey"]
app_behind_proxy = app_config.options()["behindproxy"]
rl_memory_backend = app_config.options()["limitermemory"]
app_kimai_project_id = app_config.options()["kimaiprojectid"]
app_kimai_activity_id = app_config.options()["kimaiactivityid"]
imprint_url = app_config.options()["imprinturl"]

# INIT III
# init additional functionalities

# initialize CSRF Protection,
# see: https://de.wikipedia.org/wiki/Cross-Site-Request-Forgery
# see: https://flask-wtf.readthedocs.io/en/0.15.x/csrf/
csrf = CSRFProtect(app)
# set up Rate Limiter
print("INFO: Using " + rl_memory_backend + " as RateLimiter memory backend.")
rl = RateLimit(app, rl_memory_backend).get_limiter()
# set up a shortcut for (static) methods of InputVerification class
iv = InputVerification()
# set up shortcut and initialize App Logic (Apps main Brain!)
logic = Logic(app_config.options()["kimaifullurl"])
# set ProxyFix if running behind e.g. NGINX in production
if app_behind_proxy:
    print("INFO: Application configured to run behind a Reverse Proxy.")
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)


# inject global imprint variable to jinja templates
@app.context_processor
def inject_imprint_url():
    return dict(imprint=imprint_url)


# REGISTER ERRORHANDLER
# return general error pages when flask returns with a specific http error code

# register errorhandler for "Too many requests"-response (Rate Limiter)
@app.errorhandler(429)
def rate_limited(e):
    """
     Error handler for rate limiting (429) response.

    Parameters:
    - e (Exception): The exception.

    Returns:
    - tuple: Tuple containing the rendered template and the HTTP status code (429).
    """
    return render_template('429_error.html'), 429


# register errorhandler for custom 404 error page
@app.errorhandler(404)
def page_not_found(e):
    """
     Error handler for 404 (Not Found) response.

    Parameters:
    - e (Exception): The exception.

    Returns:
    - tuple: Tuple containing the rendered template and the HTTP status code (404).
    """
    return render_template('404_error.html'), 404


# register errorhandler for custom 405 error page: Method not Found
@app.errorhandler(405)
def method_not_found(e):
    """
     Error handler for 405 (Method Not Allowed) response.

    Parameters:
    - e (Exception): The exception.

    Returns:
    - tuple: Tuple containing the rendered template and the HTTP status code (405).
    """
    return render_template('405_error.html'), 405


# register errorhandler for custom 500 error page: Internal Server Error
@app.errorhandler(500)
def server_error(e):
    """
     Error handler for 500 (Internal Server Error) response.

    Parameters:
    - e (Exception): The exception.

    Returns:
    - tuple: Tuple containing the rendered template and the HTTP status code (500).
    """
    return render_template('500_error.html'), 500


# register errorhandler for custom 502 error page: Gateway Timeout
# means: Kimai Instance unavailable
@app.errorhandler(502)
def server_error(e):
    """
    Error handler for 502 (Bad Gateway) response.

    Parameters:
    - e (Exception): The exception.

    Returns:
    - tuple: Tuple containing the rendered template and the HTTP status code (502).
    """
    return render_template('502_error.html'), 502


# register errorhandler for custom CSRF token error
@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    """
     Error handler for CSRF token error.

    Parameters:
    - e (CSRFError): The CSRFError exception.

    Returns:
    - tuple: Tuple containing the rendered template and the HTTP status code (400).
    """
    return render_template('csrf_error.html', reason=e.description), 400


# REGISTER WRAPPER FUNCTIONS
# those help to check for specific conditions when endpoint is called and reduce redundant code

# wrapper func to check whether user has a valid session
def user_session_required(f):
    """
     Wrapper function to check whether the user has a valid session.

    Parameters:
    - f (function): The function to be wrapped.

    Returns:
    - function: The wrapped function.
    """
    @wraps(f)
    def wrap(*args, **kwargs):
        if "user_session" in session:
            # add username of user to g object for display on html pages
            try:
                us = UserSession(us_json_data=session["user_session"])
                g.username = us.get_username()
            except ValueError:
                session.pop("user_session", None)  # removes the session
                flash("Fehler beim Laden der Session!")
                return redirect(url_for("home"))
            return f(*args, **kwargs)
        else:
            flash("Bitte anmelden!")
            return redirect(url_for("home"))
    return wrap


# wrapper func to check whether user requesting endpoint has no valid session yet
def no_session_required(f):
    """
    Wrapper function to check whether the user has no valid session yet.

    Parameters:
    - f (function): The function to be wrapped.

    Returns:
    - function: The wrapped function.
    """
    @wraps(f)
    def wrap(*args, **kwargs):
        if "user_session" not in session:
            return f(*args, **kwargs)
        else:
            flash("Du bist schon angemeldet!")
            return redirect(url_for("start_timesheet"))
    return wrap


# REGISTER BEFORE REQUEST FUNCTIONS
# those are called every time a request is made

# test whether corresponding kimai instance is online - if not: redirect to 502 error html-site
# acts as a downtime screen so the application only serves the pages when kimai is available as it heavily depends on it
@app.before_request
def check_kimai_availability():
    """
    Check the availability of the Kimai instance.

    Returns:
    - None: If Kimai is available.
    - abort(502): If Kimai is not available, abort with a 502 error.
    """
    # excluded endpoints: not depending on kimai instance
    excluded_endpoints = ["logout", "static", None]
    if request.endpoint not in excluded_endpoints:
        if logic.kimai_is_available() is True:
            return None
        else:
            # do not serve the website, instead show error page
            abort(502)


# REGISTER AFTER REQUEST FUNCTIONS
# those are called every time a request is made
@app.after_request
def add_custom_security_headers(resp):
    """
        Add additional security headers to all responses flask generates flask-talisman is not aware of

        Returns:
        - The incoming Response with custom security headers appended
        """
    resp.headers["Strict-Transport-Security"] = "max-age=31536000; includeSubDomains"
    resp.headers["Cross-Origin-Resource-Policy"] = "same-origin"
    resp.headers["Cross-Origin-Opener-Policy"] = "same-origin"
    resp.headers["Cross-Origin-Embedder-Policy"] = "require-corp"
    return resp


# ROUTES

# default route (root): contains the login form
@app.route('/', methods=["GET"])
@no_session_required
def home():
    """
    Default route containing the login form. Only accepts GET.

    Returns:
    - index.html: The rendered html template for the home page.
    """
    return render_template("index.html")


# route to log in user: validate credentials and create session, only POST-Requests allowed
@app.route("/login", methods=["POST"])
@rl.limit("10/minute", override_defaults=False)
@no_session_required
def login():
    """
    Route to log in the user. Only accepts POST.

    Returns:
    - redirect: Redirects to the start_timesheet() method on success or default route on failure.
    """
    req_api_token = request.form.get("password", type=str)
    if iv.pw_field_not_empty(req_api_token):
        user_session_json = logic.login(req_api_token, app_kimai_project_id, app_kimai_activity_id)
        if user_session_json is not None:
            # making session permanent to stay active during re-opened browsers etc.
            session.permanent = True
            # creating the key for the session dict with content of user session
            session["user_session"] = user_session_json
            flash("Anmeldung erfolgreich.")
            return redirect(url_for("start_timesheet"))
        else:
            flash("Falsche Anmeldedaten!")
    else:
        flash("Falsche oder keine Eingabe!")
    return redirect(url_for("home"))


# route to log out user: check for active session and remove it, only GET-Requests allowed
@app.route("/logout", methods=["GET"])
@rl.limit("10/minute", override_defaults=False)
@user_session_required
def logout():
    """
    Route to log out the user. Only accepts GET.

    Returns:
    - redirect: Redirects to default route.
    """
    if logic.timesheet_is_active(session["user_session"]) is True:
        stop_ts_return = logic.stop_timesheet(session["user_session"])
        if stop_ts_return is not None:
            flash("Aktive Zeiterfassung gestoppt & erfolgreich abgemeldet.")
        else:
            flash("Deine Zeiterfassung ist noch aktiv und konnte nicht gestoppt werden.")
            flash("Die Abmeldung war erfolgreich.")
    flash("Abmeldung erfolgreich.")
    session.pop("user_session", None)   # removes the session
    return redirect(url_for("home"))


# start timesheet route: contains the "start"-button and holds the logic to start the time
@app.route("/start", methods=["GET"])
@rl.limit("10/minute", override_defaults=False)
@user_session_required
def start_timesheet():
    """
    Route to start the timesheet. Only accepts GET.

    Returns:
    - start_time.html or redirect: Renders html template or redirects to stop_timesheet() method when already active.
    """
    confirmation = request.args.get("confirm", type=str)
    if confirmation is not None:
        confirmation.lower().strip()
        if confirmation == "true":
            confirmation = True
        else:
            confirmation = False

    if logic.timesheet_is_active(session["user_session"]) is True:
        # if statement which implements calling the same endpoint with confirm=true to start and also stop timesheet
        if confirmation is True:
            return redirect(url_for("stop_timesheet", confirm="true"))
        else:
            flash("Zeiterfassung bereits gestartet.")
            return redirect(url_for("stop_timesheet"))

    if confirmation is True:
        start_ts_return = (
            logic.start_timesheet(session["user_session"], app_kimai_project_id, app_kimai_activity_id))
        if start_ts_return is not None:
            # call start_timesheet and redirect to stop_timesheet and send flash_message on success
            session["user_session"] = start_ts_return
            flash("Zeiterfassung gestartet.")
            return redirect(url_for("stop_timesheet"))
        else:
            flash("Fehler beim Starten der Zeit.")
    return render_template("start_time.html")


# stop timesheet route: contains the "stop"-button
@app.route("/stop", methods=["GET"])
@rl.limit("10/minute", override_defaults=False)
@user_session_required
def stop_timesheet():
    """
    Route to stop the timesheet. Only accepts GET.

    Returns:
    - stop_time.html or redirect: Renders html template or redirects to start_timesheet() method.
    """
    if logic.timesheet_is_active(session["user_session"]) is False:
        flash("Noch keine Zeiterfassung gestartet.")
        return redirect(url_for("start_timesheet"))

    confirmation = request.args.get("confirm", type=str)
    if confirmation is not None:
        confirmation.lower().strip()
        if confirmation == "true":
            stop_ts_return = logic.stop_timesheet(session["user_session"])
            if stop_ts_return is not None:
                # call stop_timesheet and redirect to start_timesheet and send flash_message on success
                session["user_session"] = stop_ts_return
                flash("Zeiterfassung gestoppt.")
                return redirect(url_for("start_timesheet"))
            else:
                flash("Fehler beim Stoppen der Zeit.")
    return render_template("stop_time.html")


# API ROUTES V1

# start or stop a timesheet via GET-Request, like pressing a stop watch
@app.route("/api/v1/toggle", methods=["GET"])
@rl.limit("10/minute", override_defaults=False)
def api_v1_toggle_timesheet():
    """
    API Version 1 (REST-like):
    Route to start and stop a timesheet with credentials (api password, username)  provided via request header.

    Returns:
    JSON response with keys success (bool) and time (started, stopped, null).
    When wrong credentials provided function returns JSON response with http code 401 and success = False, time = null.
    """
    user_session_json = None
    time = None
    success = False
    # read API credentials from header
    username = request.headers.get("username", type=str)
    api_token = request.headers.get("api-token", type=str)
    if iv.field_not_empty(username) and iv.field_not_empty(api_token):
        user_session_json = logic.login(api_token, app_kimai_project_id, app_kimai_activity_id, username=username)
    if user_session_json is not None:
        if logic.timesheet_is_active(user_session_json) is True:
            if logic.stop_timesheet(user_session_json, old_api=1) is not None:
                time = "stopped"
        else:
            if logic.start_timesheet(user_session_json,
                                     app_kimai_project_id,
                                     app_kimai_activity_id,
                                     old_api=1
                                     ) is not None:
                time = "started"
    # set success = True if time could actually be stopped or started
    if time is not None:
        success = True
    response = {
        "success": success,
        "time": time
    }
    # return with status code 401 as logic.login() was not successful, probably wrong credentials
    if user_session_json is None:
        return jsonify(response), 401
    return jsonify(response)


# API ROUTES V2

# start or stop a timesheet via GET-Request, like pressing a stop watch
@app.route("/api/v2/toggle", methods=["GET"])
@rl.limit("10/minute", override_defaults=False)
def api_v2_toggle_timesheet():
    """
    API Version 2 (REST-like):
    Route to start and stop a timesheet with api bearer token provided via request header.

    Returns:
    JSON response with keys success (bool) and time (started, stopped, null).
    When wrong credentials provided function returns JSON response with http code 401 and success = False, time = null.
    """
    user_session_json = None
    time = None
    success = False
    # read API Bearer Token from header
    api_token = request.headers.get("api-token", type=str)
    if iv.field_not_empty(api_token):
        user_session_json = logic.login(api_token, app_kimai_project_id, app_kimai_activity_id)
    if user_session_json is not None:
        if logic.timesheet_is_active(user_session_json) is True:
            if logic.stop_timesheet(user_session_json) is not None:
                time = "stopped"
        else:
            if logic.start_timesheet(user_session_json, app_kimai_project_id, app_kimai_activity_id) is not None:
                time = "started"
    # set success = True if time could actually be stopped or started
    if time is not None:
        success = True
    response = {
        "success": success,
        "time": time
    }
    # return with status code 401 as logic.login() was not successful, probably wrong credentials
    if user_session_json is None:
        return jsonify(response), 401
    return jsonify(response)


if __name__ == '__main__':
    app.run()
