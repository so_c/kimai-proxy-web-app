import requests


# Class represents a client to the Kimai instance API. It calls the endpoints to log in, start and stop the time, ... .
class KimaiAPIClient:
    """
    Provides functionality to interact with the Kimai REST-API.
    """
    # constructor of class
    # expected kimai_url format: "https://kimaiinstance.tld", if one trailing slash exists it will be removed here
    def __init__(self, api_token: str, kimai_url: str, username: str = None):
        """
        Constructor for the KimaiAPIClient class.

        Parameters:
        - api_token (str): The API token (since Kimai 2.14.0: Bearer Token) for Kimai authentication.
        - kimai_url (str): The root URL of the Kimai instance.
        - optional: username (str, default: None): The username for old Kimai api authentication method.
        """
        self._x_auth_api_token = api_token
        self._kimai_root_url = kimai_url.removesuffix("/")
        if username is None:
            self._request_header = {
                "Authorization": "Bearer " + self._x_auth_api_token,
                "accept": "application/json"
            }
        else:
            # OLD KIMAI API - will be removed
            self._request_header = {
                "X-AUTH-USER": username,
                "X-AUTH-TOKEN": self._x_auth_api_token,
                "accept": "application/json"
            }

    # test, if username and api token are correct, returns unique user id of that user on success, else 0
    def check_credentials(self):
        """
          Test for correct user credentials.

        Returns:
        - int: User ID on success, 0 on failure.
        """
        api_endpoint = "/api/users/me"
        full_uri = self._kimai_root_url + api_endpoint
        try:
            response = requests.get(full_uri, headers=self._request_header, timeout=15)
            response.raise_for_status()
            if response.status_code == 200:
                json_response = response.json()
                return json_response.get("id")
            else:
                # returns 0 when credentials wrong
                return 0
        except requests.exceptions.RequestException as e:
            print("Error in Kimai_API_Client Module [check_credentials()]: ", e)
            return 0
        except requests.exceptions.JSONDecodeError as e:
            print("Error in Kimai_API_Client Module [check_credentials()]: ", e)
            return 0

    # Create and start a timesheet, on success: returns timesheet it, on failure: returns 0
    def start_timesheet(self, project_id, activity_id):
        """
        Start a new timesheet.

        Parameters:
        - project_id (int): The ID of the project.
        - activity_id (int): The ID of the activity.

        Returns:
        - int: Timesheet ID on success, 0 on failure.
        """
        api_endpoint = "/api/timesheets"
        data = {
            "activity": activity_id,
            "project": project_id,
            "exported": False,
            "billable": True
        }
        full_uri = self._kimai_root_url + api_endpoint
        try:
            response = requests.post(full_uri, headers=self._request_header, json=data, timeout=15)
            response.raise_for_status()
            if response.status_code == 200:
                json_response = response.json()
                return json_response.get("id")
            else:
                return 0
        except requests.exceptions.RequestException as e:
            print("Error in Kimai_API_Client Module [start_timesheet()]: ", e)
            return 0
        except requests.exceptions.JSONDecodeError as e:
            print("Error in Kimai_API_Client Module [start_timesheet()]: ", e)
            return 0

    # Stop an active timesheet, on success: returns True, else: returns False
    def stop_timesheet(self, timesheet_id):
        """
        Stop an active timesheet.

        Parameters:
        - timesheet_id (int): The ID of the timesheet to stop.

        Returns:
        - bool: True on success, False on failure.
        """
        api_endpoint = f"/api/timesheets/{timesheet_id}/stop"
        full_uri = self._kimai_root_url + api_endpoint
        try:
            response = requests.get(full_uri, headers=self._request_header, timeout=15)
            response.raise_for_status()
            if response.status_code == 200:
                return True
            else:
                return False
        except requests.exceptions.RequestException as e:
            print("Error in Kimai_API_Client Module [stop_timesheet()]: ", e)
            return False

    # Test general connection to kimai login, on success: returns True, else: returns False
    def test_connection(self):
        """
        Test the general connection to the Kimai instance without any credentials.

        Returns:
        - bool: True on success, False on failure.
        """
        api_endpoint = "/"
        full_uri = self._kimai_root_url + api_endpoint
        try:
            response = requests.get(full_uri, timeout=15)
            response.raise_for_status()
            if response.status_code == 200:
                return True
            else:
                return False
        except requests.exceptions.RequestException as e:
            print("Error in Kimai_API_Client Module [test_connection()]: ", e)
            return False

    # check for active timesheet, returns tuple of timesheet_id, belonging activity_id and project_id if there is any
    # else: return 0, 0, 0
    # assumes, that there can only be one active timesheet at a time!
    # which is the default set in the kimai instance settings
    def user_timesheet_active(self) -> tuple[int, int, int]:
        """
        Check for an active timesheet.

        Returns:
        - tuple[int, int, int]: Tuple of timesheet_id, activity_id, and project_id if active timesheet exists,
        else 0, 0, 0.
        """
        api_endpoint = "/api/timesheets/active"
        full_uri = self._kimai_root_url + api_endpoint
        try:
            response = requests.get(full_uri, headers=self._request_header, timeout=15)
            response.raise_for_status()
            if response.status_code == 200:
                json_response = response.json()
                if len(json_response) == 1:
                    return (json_response[0].get("id"),     # ts id
                            json_response[0].get("activity").get("id"),     # activity id
                            json_response[0].get("activity").get("project").get("id"))  # project id
                elif len(json_response) == 0:
                    return 0, 0, 0
                # error, length greater than 1 (should not be possible if set right in kimai settings)
                else:
                    print("Error in Kimai_API_Client Module [user_timesheet_active()]: More than one active Timesheet!")
                    return 0, 0, 0
            else:
                return 0, 0, 0
        except requests.exceptions.RequestException as e:
            print("Error in Kimai_API_Client Module [user_timesheet_active()]: ", e)
            return 0, 0, 0
        except requests.exceptions.JSONDecodeError as e:
            print("Error in Kimai_API_Client Module [user_timesheet_active()]: ", e)
            return 0, 0, 0

    # getter for username
    def get_username(self):
        """
        Get the username of current instance of class.

        Returns:
        - str: The username.
        """
        api_endpoint = "/api/users/me"
        full_uri = self._kimai_root_url + api_endpoint
        try:
            response = requests.get(full_uri, headers=self._request_header, timeout=15)
            response.raise_for_status()
            if response.status_code == 200:
                json_response = response.json()
                return json_response.get("username")
            else:
                # returns 0 when credentials wrong or some other error
                return 0
        except requests.exceptions.RequestException as e:
            print("Error in Kimai_API_Client Module [get_username()]: ", e)
            return 0
        except requests.exceptions.JSONDecodeError as e:
            print("Error in Kimai_API_Client Module [get_username()]: ", e)
            return 0

    # getter for api token
    def get_token(self):
        """
        Get the API token of current instance of class.

        Returns:
        - str: The API token.
        """
        return self._x_auth_api_token
